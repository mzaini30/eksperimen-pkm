PKMP

Program Kreativitas Mahasiswa Penelitian
Psikoweb.xyz
Menulis Ekspresif dan Musik Relaksasi Menurunkan Tingkat Stres Melalui Media Psikoweb

Logo perguruan tinggi

Latar Belakang (kalau bisa, merupakan data) atau abstrak

Latar Belakang

Tahun pertama merupakan masa-masa kritis bagi kehidupan mahasiswa. Masa tersebut merupakan masa transisi dan penyesuaiaan terhadap tuntutan sosial dan akademis universitas, masa-masa dengan kemungkinan besar untuk dropout dan perubahan pembelajaran. (Stavrianopoulus dalam Spott, 2011)

Tujuan

Tujuan dari penelitian ini adalah untuk mengetahui tingkat efektifitas dari media psikoweb terhadap manajemen stres mahasiswa baru di universitas Samarinda.

Metode

Penelitian ini dilakukan dengan menggunakan rancangan eksperimen. Subyek dimasukkan ke dalam kelompok eksperimen berdasarkan hasil skala stres dan dikenai pre test dan post test. Setelah itu dilakukan manipulasi yang bentuk perlakuannya adalah melalui menulis ekspresif atau mendengarkan musik relaksasi dengan media psikoweb. Selanjutnya dilakukan pengukuran ulang (post test) dan tindak lanjut (follow up) pada variabel stres. Berdasarkan uraian kegiatan yang digunakan dalam penelitian ini, maka desain penelitian yang digunakan adalah Treatment by Level Design (T-L).

Gambar desain penelitian (pakai erd)

Hasil (infografis dan foto)

Hasil

Kesimpulan dan saran

Referensi

Arikunto, S. 2010. Prosedur Penelitian: Suatu Pendekatan Praktik. Jakarta: Rineka Cipta.

Campbell, D. 2001. Efek Mozart Memanfaatkan Kekuatan Musik untuk Mempertajam Pikiran, Meningkatkan Kreativitas, dan Menyehatkan Tubuh. Jakarta: PT Gramedia Utama.
		
Djohan. 2006. Terapi Musik: Teori dan Aplikasi. Yogyakarta: Galang Press

Hidayat, R. 2010. Cara Praktis Membangun Website Gratis: Pengertian Website. Jakarta: PT Elex Media Komputindo Kompas Granedia

Intan, A.D. 2012. Occupational Stress Management with Group Intervention for Parenting Division in Campus Diakonea Modern (KDM). Tesis.

Kemper, K.J. dan Danhauer, S. C. 2005. Music as Therapy. Southern Medical Association.

Markam, S. dan Slamet, S. 2008. Pengantar Psikologi Klinis. Jakarta: UI Press

Pennebaker, J.W. dan Chung, C.K. 2007. Expressive writing: Connections to Physical and Mental Health. Oxford Handbook of Health Phychology. New York: Oxford University Press.

Sarafino, E.P. dan Timothy W.S. 2012. Health psychology, biopsychosocial interactions, 7th edition. New Jersey: Jhon Willey & Sons, Inc

Spott, J. 2011. Thesis Social and Academic Adaptability of First Year Freshmen Students. New York: Mc Graw Hill.

Tarigan, H.G. 2008. Membaca sebagai Suatu Keterampilan Berbahasa. Bandung: Angkasa.

Widiono, H. 2005. Bahasa Indonesia. Mata Kuliah Pengembangan Kepribadian di Perguruan Tinggi. Jakarta: PT. Gramedia Widiasarana.


Detail kontak:

Tim
Muhammad Zaini (1502105051)
Chairini Fathonah (1502105063)
Mia Satriani (1502105045)

Dosen Pembimbing
Hairani Lubis, S.Psi., M.Psi., Psikolog

CP. 081545143654
Email. muhzaini30@gmail.com

Tanggal dan waktu penelitian